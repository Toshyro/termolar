/*****************************************************************************
** Programa..............: QlikControladoriaDelete.i
** Descricao.............: Pr�-Processador para Limpar Tabelas de Controladoria
** Versao................: 1.01.00.001
** Data Geracao..........: 01/01/2017
** Criado por............: Juan Felipe Trindade de Almeida - JP2B
** Criado em.............: 01/01/2017
*****************************************************************************/

FOR EACH {1} FIELDS() EXCLUSIVE-LOCK
    {2}:

    DELETE {1} NO-ERROR.
END.
